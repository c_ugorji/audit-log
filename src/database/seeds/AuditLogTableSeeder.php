<?php

use Gloopro\Audit\Models\AuditLog;
use Illuminate\Database\Seeder;

class AuditLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AuditLog::class, 10)->create();
    }
}
