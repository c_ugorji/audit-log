<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Gloopro\Audit\Models\AuditLog;

$factory->define(AuditLog::class, function (Faker $faker) {
    $req = [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->email,
        'phone_number' => $faker->phoneNumber,
        'company_id' => $faker->randomDigit,
        'role_id' => $faker->randomDigit,
        'password' => bcrypt('secret'),
    ];

    $resp = [
        'message' => 'Successful',
        'user' => $req
    ];

    return [
        // refactoring factory file to use the constants created in the AuditLog model
        AuditLog::USER_ID => $faker->randomNumber,
        AuditLog::ROLE_ID => $faker->randomNumber,
        AuditLog::COMPANY_ID => $faker->randomNumber,
        AuditLog::APP_NAME => $faker->domainName,
        AuditLog::DOMAIN => $faker->domainWord,
        AuditLog::EVENT_NAME => $faker->word,
        AuditLog::EVENT_TYPE => $faker->word,
        AuditLog::DESCRIPTION => $faker->sentence(5),
        AuditLog::LOCATION => $faker->ipv4,
        AuditLog::REQUEST_OBJ => json_encode($req),
        AuditLog::RESPONSE_OBJ => json_encode($resp),
        AuditLog::OLD_RECORD => null,
        AuditLog::NEW_RECORD => null
    ];
});
