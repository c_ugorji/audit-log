<?php

use Gloopro\Audit\Models\AuditLog;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AuditLog::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements(AuditLog::ID);
            $table->integer(AuditLog::USER_ID);
            $table->integer(AuditLog::ROLE_ID);
            $table->integer(AuditLog::COMPANY_ID);

            // $table->unsignedBigInteger(AuditLog::USER_ID);
            // $table->foreign(AuditLog::USER_ID)->references(AuditLog::USERS_TABLE_PRIMARY_KEY)->on(AuditLog::USERS_TABLE_NAME);

            // $table->unsignedBigInteger(AuditLog::ROLE_ID);
            // $table->foreign(AuditLog::ROLE_ID)->references(AuditLog::ROLES_TABLE_PRIMARY_KEY)->on(AuditLog::ROLES_TABLE_NAME);

            // $table->unsignedBigInteger(AuditLog::COMPANY_ID);
            // $table->foreign(AuditLog::COMPANY_ID)->references(AuditLog::COMPANIES_TABLE_PRIMARY_KEY)->on(AuditLog::COMPANIES_TABLE_NAME);

            $table->string(AuditLog::APP_NAME);
            $table->string(AuditLog::DOMAIN);
            $table->string(AuditLog::EVENT_NAME);
            $table->string(AuditLog::EVENT_TYPE);
            $table->longText(AuditLog::DESCRIPTION);
            $table->string(AuditLog::LOCATION)->nullable();
            $table->longText(AuditLog::REQUEST_OBJ);
            $table->longText(AuditLog::RESPONSE_OBJ);
            $table->longText(AuditLog::OLD_RECORD)->nullable();
            $table->longText(AuditLog::NEW_RECORD)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AuditLog::TABLE_NAME);
    }
}
