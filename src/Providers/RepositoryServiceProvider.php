<?php

namespace Gloopro\Audit\Providers;

use Gloopro\Audit\Repositories\AuditLogRepository;
use Gloopro\Audit\Repositories\Contracts\AuditLogRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public $bindings = [
        AuditLogRepositoryInterface::class => AuditLogRepository::class,
    ];
}
