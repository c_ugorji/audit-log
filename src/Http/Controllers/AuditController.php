<?php

namespace Gloopro\Audit\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Gloopro\Audit\Http\Resources\AuditLogResource;
use Gloopro\Audit\Http\Resources\AuditLogResourceCollection;
use Gloopro\Audit\Models\AuditLog;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class AuditController extends Controller
{
    public function index() //: AuditLogResourceCollection
    {
        try{
            $records = AuditLog::all();
            //$records = AuditLog::paginate();
            if($records->isEmpty()){
                // throw new Exception('No record found', 404);
                throw new Exception('No record found', Response::HTTP_NOT_FOUND);
            }
            // return (new AuditLogResourceCollection($records))->response()->status(200);
            return (new AuditLogResourceCollection($records))->response()->setStatusCode(Response::HTTP_OK);
        }
        catch(Exception $ex){
            return $this->createErrorResponse($ex);
        }
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            AuditLog::USER_ID => 'required',
            AuditLog::ROLE_ID => 'required',
            AuditLog::COMPANY_ID => 'required',
            AuditLog::APP_NAME => 'required',
            AuditLog::DOMAIN => 'required',
            AuditLog::EVENT_NAME => 'required',
            AuditLog::EVENT_TYPE => 'required',
            AuditLog::DESCRIPTION => 'required',
            AuditLog::REQUEST_OBJ => 'required',
            AuditLog::RESPONSE_OBJ => 'required'
        ]);

        try{
            if($validator->fails()){
                // throw new Exception('Validation Failed', 422);
                throw new Exception('Validation Failed', Response::HTTP_BAD_REQUEST);
            }

            $record = new AuditLog();
            $record->setAttribute(AuditLog::USER_ID, $request->get(AuditLog::USER_ID));
            $record->setAttribute(AuditLog::ROLE_ID, $request->get(AuditLog::ROLE_ID));
            $record->setAttribute(AuditLog::COMPANY_ID, $request->get(AuditLog::COMPANY_ID));
            $record->setAttribute(AuditLog::APP_NAME, $request->get(AuditLog::APP_NAME));
            $record->setAttribute(AuditLog::DOMAIN, $request->get(AuditLog::DOMAIN));
            $record->setAttribute(AuditLog::EVENT_NAME, $request->get(AuditLog::EVENT_NAME));
            $record->setAttribute(AuditLog::EVENT_TYPE, $request->get(AuditLog::EVENT_TYPE));
            $record->setAttribute(AuditLog::DESCRIPTION, $request->get(AuditLog::DESCRIPTION));
            $record->setAttribute(AuditLog::LOCATION, $request->get(AuditLog::LOCATION));
            $record->setAttribute(AuditLog::REQUEST_OBJ, $request->get(AuditLog::REQUEST_OBJ));
            $record->setAttribute(AuditLog::RESPONSE_OBJ, $request->get(AuditLog::RESPONSE_OBJ));
            $record->setAttribute(AuditLog::OLD_RECORD, $request->get(AuditLog::OLD_RECORD));
            $record->setAttribute(AuditLog::NEW_RECORD, $request->get(AuditLog::NEW_RECORD));

            $record->save();

            print_r($record);

            // return response('Record successfully logged', 201);
            // return (new AuditLogResource($record))->response()->status(201);
            return (new AuditLogResource($record))->response()->setStatusCode(Response::HTTP_CREATED);
        }
        catch(Exception $exception){
            return $this->createErrorResponse($exception);
        }

    }

    /**
     * @param id id of a particular audit record
     * @return AuditLogResource properly formatted audit log 
     */
    public function show($id) //: AuditLogResource 
    {
        try{
            $record = AuditLog::find($id);
            if(!$record){
                throw new Exception('No record found', Response::HTTP_NOT_FOUND);
            }
            // return (new AuditLogResource($record))->response()->status(200);
            return (new AuditLogResource($record))->response()->setStatusCode(Response::HTTP_FOUND);
        }
        catch(Exception $ex){
            return $this->createErrorResponse($ex);
        }
    }

    public function getLogsByAppNameAndCompanyId($app_name, $company_id)
    {
        try{
            $records = AuditLog::where(AuditLog::APP_NAME, $app_name)->where(AuditLog::COMPANY_ID, $company_id)->get();

            if($records->isEmpty()){
                throw new Exception('No record found', Response::HTTP_NOT_FOUND);
            }

            return (new AuditLogResourceCollection($records))->response()->setStatusCode(Response::HTTP_OK);
        }
        catch(Exception $ex){
            return $this->createErrorResponse($ex);
        }
    }

    /**
     *
     * Create and return error/exception response
     * @param Exception $exception
     *
     * @return \Illuminate\Http\Response
     *
     */
    private function createErrorResponse(Exception $exception) {
        return response(['errorCode' => $exception->getCode(), 'errorMessage' => $exception->getMessage()], 500);
    }
}