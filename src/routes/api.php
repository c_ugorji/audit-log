<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Gloopro\Audit\Http\Controllers'], function(){
    
    Route::get('api/audit', 'AuditController@index');

    Route::post('api/audit', 'AuditController@store');

    Route::get('api/audit/{id}', 'AuditController@show');

    Route::get('api/audit/app/{app_name}/company/{company_id}', 'AuditController@getLogsByAppNameAndCompanyId');
});

