<?php

namespace Gloopro\Audit\Repositories;

use Gloopro\Audit\Repositories\Contracts\AuditLogRepositoryInterface;
use Gloopro\Audit\Models\AuditLog;

class AuditLogRepository implements AuditLogRepositoryInterface
{
    public function addAuditLog(AuditLog $auditLogModel){
        $auditLogModel->save();
        return $auditLogModel;
    }

    public function getAllAuditLog(){
        return AuditLog::all();
    }

    public function getAuditLogById($id){
        return AuditLog::find($id);
    }

    public function getAuditLogByCompanyId($company_id){
        return AuditLog::where(AuditLog::COMPANY_ID, $company_id)->get();
    }

    public function setupModel($domain, $eventName, $eventType, $description, $locationIP, $requestObject, $responseObject, 
    $oldRecord, $newRecord, $userId, $roleId, $companyId, $appName){
        $record = new AuditLog();
        $record->setAttribute(AuditLog::USER_ID, $userId);
        $record->setAttribute(AuditLog::ROLE_ID, $roleId);
        $record->setAttribute(AuditLog::COMPANY_ID, $companyId);
        $record->setAttribute(AuditLog::APP_NAME, $appName);
        $record->setAttribute(AuditLog::DOMAIN, $domain);
        $record->setAttribute(AuditLog::EVENT_NAME, $eventName);
        $record->setAttribute(AuditLog::EVENT_TYPE, $eventType);
        $record->setAttribute(AuditLog::DESCRIPTION, $description);
        $record->setAttribute(AuditLog::LOCATION, $locationIP);
        $record->setAttribute(AuditLog::REQUEST_OBJ, $requestObject);
        $record->setAttribute(AuditLog::RESPONSE_OBJ, $responseObject);
        $record->setAttribute(AuditLog::OLD_RECORD, $oldRecord);
        $record->setAttribute(AuditLog::NEW_RECORD, $newRecord);
        return $record;
    }
}