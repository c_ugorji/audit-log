<?php

namespace Gloopro\Audit\Repositories\Contracts;

use Gloopro\Audit\Models\AuditLog;

interface AuditLogRepositoryInterface
{
    /**
     * Method to add record to audit log 
     * @param App\AuditLog $auditLogModel AuditLog model object with details required to create an audit log
     * @return AuditLog Created audit log object
     */
    public function addAuditLog(AuditLog $auditLogModel);

    /**
     * Gets all audit log from the database
     *
     * @return Collection
     */
    public function getAllAuditLog();

    // public function getAllAuditLogUsingPagination();
    
    public function getAuditLogById($id);

    // public function getAuditLogByIdUsingPagination($id);

    public function getAuditLogByCompanyId($company_id);

    // public function getAuditLogByCompanyIdUsingPagination($company_id);
    
    public function setupModel($domain, $eventName, $eventType, $description, $locationIP, $requestObject, $responseObject, 
    $oldRecord, $newRecord, $userId, $roleId, $companyId, $appName);
}
