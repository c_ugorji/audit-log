<?php

namespace Gloopro\Audit\Models;

use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    //table name
    const TABLE_NAME = 'audit_logs';
    // const USERS_TABLE_NAME = 'users';
    // const COMPANIES_TABLE_NAME = 'companies';
    // const ROLES_TABLE_NAME = 'roles';

    //table field names
    const ID = 'id';
    const USER_ID = 'user_id';
    const ROLE_ID = 'role_id';
    const COMPANY_ID = 'company_id';
    // const USERS_TABLE_PRIMARY_KEY = 'id';
    // const ROLES_TABLE_PRIMARY_KEY = 'id';
    // const COMPANIES_TABLE_PRIMARY_KEY = 'id';
    const APP_NAME = 'app_name';
    const DOMAIN = 'domain';
    const EVENT_NAME = 'event_name';
    const EVENT_TYPE = 'event_type';
    const DESCRIPTION = 'description';
    const LOCATION = 'location';
    const REQUEST_OBJ = 'request';
    const RESPONSE_OBJ = 'response';
    const OLD_RECORD = 'old_record';
    const NEW_RECORD = 'new_record';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    //api pagination response field name
    const CURRENT_PAGE = 'current_page';
    const DATA = 'data';
    const FIRST_PAGE_URL = 'first_page_url';
    const FROM = 'from';
    const LAST_PAGE = 'last_page';
    const LAST_PAGE_URL = 'last_page_url';
    const NEXT_PAGE_URL = 'next_page_url';
    const PATH = 'path';
    const PER_PAGE = 'per_page';
    const PREV_PAGE_URL = 'prev_page_url';
    const TO = 'to';
    const TOTAL = 'total';
}
