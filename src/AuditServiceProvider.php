<?php

namespace Gloopro\Audit;

use Illuminate\Support\ServiceProvider;

class AuditServiceProvider extends ServiceProvider
{
    public function boot(){
        //this will load the api routes from the routes folder in the src directory
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        //this will load all migration file from the package folder database/migrations when php artisan migrate is executed
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        //this will load all factory file from the package folder database/factories
        $this->loadFactoriesFrom(__DIR__ . '/database/factories');
        
    }

    public function register()
    {
        
    }
}